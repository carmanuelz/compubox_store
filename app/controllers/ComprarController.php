<?php

use CompuboxStore\Entities\Venta;
use CompuboxStore\Entities\Cliente;
use CompuboxStore\Entities\Direccionenvio;
use CompuboxStore\Entities\Ventaproducto;
use CompuboxStore\Entities\Ticket;
use CompuboxStore\Repositories\ProductoRepo;
use CompuboxStore\Repositories\VentasRepo;

class ComprarController extends BaseController{

    protected $productoRepo;
    protected $ventasRepo;
    protected $cryptkey = "qwerty";

    public function __construct(ProductoRepo $productoRepo, VentasRepo $ventasRepo)
    {
        $this->productoRepo = $productoRepo;
        $this->ventasRepo = $ventasRepo;
    }

    public function index()
    {
        return View::make('client/comprarform', array('nameView' => 'Comprar | '));
    }

    public function registrar_venta()
    {
        $data  = Input::all();
        $datacliente = Input::only(['nombre','apellido','telefono','dni','email']);
        $datadireccionenvio = Input::only(['direccion','ubigeo_id']);
        $cantidad = 1;//$data['cantidad'];
        $rules = [
            'nombre'    => 'required',
            'apellido'  => 'required',
            'telefono'  => 'required|numeric',
            'dni'       => 'required|numeric',
            'email'     => 'required|email|confirmed',
            'email_confirmation' => 'required',
            'direccion' => 'required',
        ];

        $validation = \Validator::make($data, $rules);

        if($validation->passes())
        {
            $producto = $this->productoRepo->getActive();

            $cliente = Cliente::create($datacliente);

            $direccionenvio = Direccionenvio::create($datadireccionenvio);

            $codigo = '';
            do{
                $codigo = str_random(8);
            }while($this->ventasRepo->getByCode($codigo) != null);

            DB::beginTransaction();
            $venta = new Venta();
            $venta->fecha = new DateTime;
            $venta->cliente_id = $cliente->id;
            $venta->direccionenvio_id = $direccionenvio->id;
            $venta->tipo = 'ticket';
            $venta->total = $producto->precio_venta*$cantidad;
            $venta->codigo = $codigo;
            $venta->estado = 'mail_no_verificado';
            $venta->save();

            $ventaproducto = new Ventaproducto();
            $ventaproducto->precio_unitario = $producto->precio_venta;
            $ventaproducto->producto_id = $producto->id;
            $ventaproducto->venta_id = $venta->id;
            $ventaproducto->cantidad = $cantidad;
            $ventaproducto->save();

            $ticket = new Ticket();
            $ticket->codigo = $codigo;
            $ticket->save();

            $message = null;
            $fromEmail = 'carmanuel.zarate@gmail.com';
            $fromName = 'Carlos Zarate';
            try
            {
                Mail::send('emails.client.verificacionmail', array('url'=>URL::route('verificacion',Crypt::encrypt($codigo, $this->cryptkey))), function($message) use ($fromEmail, $fromName)
                {
                    $toEmail = Input::get('email');
                    $toName = Input::get('nombre')." ".Input::get('apellido');
                    $message->to($toEmail, $toName);
                    $message->from($fromEmail,$fromName);
                    $message->subject('verificaion');
                });
                DB::commit();
            }catch (\Exception $e)
            {
                DB::rollback();
                throw $e;
            }
            return Redirect::route('home_compubox');
        }

        return Redirect::back()->withInput()->withErrors($validation);
    }

    public function verificacion($codigo)
    {
        $decrypted = Crypt::decrypt($codigo, $this->cryptkey);
        $venta = $this->ventasRepo->getByCOde($decrypted);
        $venta->estado = "mail_verificado";
        $venta->save();
        return View::make('client/mailverificacion',array('nameView' => 'Ventas |','venta'=>$venta));
    }
}