<?php

class AuthController extends BaseController{

    public function index()
    {
        return View::make('admin/signin');
    }

    public function login()
    {
        $data = Input::only('email', 'password', 'remember');

        $credential = [ 'email' => $data['email'], 'password' => $data['password']];

        if(Auth::attempt($credential, $data['remember']))
        {
            return Redirect::route('admin_home');
        }

        return Redirect::back()->with('login_error',1);
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::route('home');
    }
} 