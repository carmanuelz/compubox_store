<?php

use CompuboxStore\Repositories\VentasRepo;
use \CompuboxStore\Repositories\TicketRepo;

class TicketController extends BaseController{

    protected $ventasRepo;
    protected $ticketRepo;

    public function __construct(VentasRepo $vetnasRepo, TicketRepo $ticketRepo)
    {
        $this->ventasRepo = $vetnasRepo;
        $this->ticketRepo = $ticketRepo;
    }

    public function index()
    {
        return View::make('client/ticketform', array('nameView' => 'Confirmar Pago'));
    }

    public function validar()
    {
        $data  = Input::all();
        $dataticket = Input::only(['codigo','nro_operacion','fecha_confirmacion','monto']);

        $rulescodigo = [
            'codigo'    => 'required|existventa'
        ];

        \Validator::extend('equal', function($attribute, $value, $parameters)
        {
            return $value == $parameters[0];
        });

        \Validator::extend('existventa', function($attribute, $value, $parameters)
        {
            $venasRepo = new VentasRepo();
            if($venasRepo->getByCode($value) != null)
                return true;
            return false;
        });
        $validationcodigo = \Validator::make($data, $rulescodigo);

        if($validationcodigo->passes())
        {
            $venta = $this->ventasRepo->getByCode($dataticket['codigo']);
            $rules = [
                'nro_operacion' => 'required|numeric',
                'monto'    => 'required|equal:'.$venta->total
            ];
            $validation = \Validator::make($data, $rules);
            if($validation->passes())
            {
                DB::beginTransaction();

                $ticket = $this->ticketRepo->getByCode($dataticket['codigo']);
                $ticket->monto = $dataticket['monto'];
                $ticket->nro_operacion = $dataticket['nro_operacion'];
                $ticket->fecha_confirmacion = $dataticket['fecha_confirmacion'];
                $ticket->save();
                $venta->estado = 'pago_no_verificado';
                $venta->save();

                $message = null;
                $fromEmail = 'carmanuel.zarate@gmail.com';
                $fromName = 'Carlos Zarate';
                try
                {
                    Mail::send('emails.admin.verificacionpagomail', array('codigo'=>$dataticket['codigo']), function($message) use ($fromEmail, $fromName)
                    {
                        $message->to($fromEmail, $fromName);
                        $message->from($fromEmail,$fromName);
                        $message->subject('verificaion pago');
                    });
                    DB::commit();
                }catch (\Exception $e)
                {
                    DB::rollback();
                    throw $e;
                }
                return Redirect::route('comprar');
            }
            else
                return Redirect::back()->withInput()->withErrors($validation);
        }

        return Redirect::back()->withInput()->withErrors($validationcodigo);
    }
} 