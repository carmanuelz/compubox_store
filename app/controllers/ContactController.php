<?php

class ContactController extends BaseController {

    public function index()
    {
        return View::make('client/contact', array('nameView' => 'Contacto | '));
    }

    public function send()
    {
        $data = Input::only('name', 'email', 'subject', 'message');

        $message = null;
        $fromEmail = 'carmanuel.zarate@gmail.com';
        $fromName = 'Carlos Zarate';

        Mail::send('emails.admin.contactomail', $data, function($message) use ($fromEmail, $fromName)
        {
            $message->to($fromEmail, $fromName);
            $message->from('clindy.26@gmail.com','Claudia Valdivieso');
            $message->subject('contacto');
        });
        return Redirect::back();
    }
}
