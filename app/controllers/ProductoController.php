<?php
use CompuboxStore\Repositories\ProductoRepo;

class ProductoController extends BaseController{

    protected $productoRepo;

    public function __construct(ProductoRepo $productoRepo)
    {
        $this->productoRepo = $productoRepo;
    }

    public function administrar()
    {
        $producto = $this->productoRepo->getActive();
        return View::make('admin/administrarproducto', compact('producto'));
    }

    public function update()
    {
        $data  = Input::all();
        $dataproducto = Input::only(['descripcion','so','almacenamiento', 'memorias', 'cpu', 'gpu', 'otros']);
        $rules = [
            'descripcion'    => 'required',
            'so'  => 'required',
            'almacenamiento'  => 'required',
            'memorias'  => 'required',
            'cpu'  => 'required',
            'gpu'  => 'required',
            'gpu'  => 'required',
        ];
        $producto = $this->productoRepo->getActive();

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            $producto->fill($dataproducto);
            $producto->save();
            return Redirect::route('administrar_producto');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }
} 