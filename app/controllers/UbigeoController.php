<?php

use CompuboxStore\Repositories\UbigeoRepo;

class UbigeoController extends BaseController{

    protected $ubigeoRepo;

    public function __construct(UbigeoRepo $ubigeoRepo)
    {
        $this->ubigeoRepo = $ubigeoRepo;
    }

    public function getubigeos()
    {
        return Response::json($this->ubigeoRepo->get());
    }
} 