<?php

use CompuboxStore\Repositories\CompuboxstorecontentRepo;
use Equom\Repositories\EquomcontentRepo;
use EquomPresencial\Repositories\EquompresencialcontentRepo;
use CompuboxStore\Repositories\CompuboximageRepo;
use Equom\Repositories\EquomimageRepo;
use EquomPresencial\Repositories\EquompresencialimageRepo;

class AdminController extends BaseController{

    protected $compuboxstorecontentRepo;
    protected $equomcontentRepo;
    protected $equompresencialcontentRepo;
    protected $compuboximageRepo;
    protected $equomimageRepo;
    protected $equompresencialimage;

    public function __construct(
        CompuboxstorecontentRepo $compuboxstorecontentRepo,
        EquomcontentRepo $equomcontentRepo,
        EquompresencialcontentRepo $equompresencialcontentRepo,
        CompuboximageRepo $compuboximageRepo,
        EquomimageRepo $equomimageRepo,
        EquompresencialimageRepo $equompresencialimageRepo)
    {
        $this->compuboxstorecontentRepo = $compuboxstorecontentRepo;
        $this->equomcontentRepo = $equomcontentRepo;
        $this->equompresencialcontentRepo = $equompresencialcontentRepo;
        $this->compuboximageRepo = $compuboximageRepo;
        $this->equomimageRepo = $equomimageRepo;
        $this->equompresencialimageRepo = $equompresencialimageRepo;
    }

    public function index()
    {
        return View::make('admin/home');
    }

    public function contentEquom()
    {
        $contenidos = $this->equomcontentRepo->get();
        return View::make('admin/content-equom', compact("contenidos"));
    }

    public function contentEquomPresencial()
    {
        $contenidos = $this->equompresencialcontentRepo->get();
        return View::make('admin/content-equom-presencial', compact("contenidos"));
    }

    public function contentCompubox()
    {
        $contenidos = $this->compuboxstorecontentRepo->get();
        return View::make('admin/content-compubox', compact("contenidos"));
    }

    public function formcontentCompubox($id)
    {
        $contenido = $this->compuboxstorecontentRepo->get($id);
        return View::make('admin/editcontentform', array("contenido" => $contenido, "tipo_contenido" => "Compubox", "route" => "update_content_compubox"));
    }

    public function formcontentEquom($id)
    {
        $contenido = $this->equomcontentRepo->get($id);
        return View::make('admin/editcontentform', array("contenido" => $contenido, "tipo_contenido" => "Equom", "route" => "update_content_equom"));
    }

    public function formcontentEquomPresencial($id)
    {
        $contenido = $this->equompresencialcontentRepo->get($id);
        return View::make('admin/editcontentform', array("contenido" => $contenido, "tipo_contenido" => "Equom Presencial", "route" => "update_content_equompresencial"));
    }

    public function updatecontentCompubox()
    {
        $data  = Input::all();
        $datacontent = Input::only(['id','descripcion','content']);
        $rules = [
            'descripcion'    => 'required',
            'content'  => 'required',
        ];
        $contenido = $this->compuboxstorecontentRepo->get($datacontent['id']);

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            $contenido->fill($datacontent);
            $contenido->save();
            return Redirect::route('content_compubox');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

    public function updatecontentEquom()
    {
        $data  = Input::all();
        $datacontent = Input::only(['id','descripcion','content']);
        $rules = [
            'descripcion'    => 'required',
            'content'  => 'required',
        ];
        $contenido = $this->equomcontentRepo->get($datacontent['id']);

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            $contenido->fill($datacontent);
            $contenido->save();
            return Redirect::route('content_equom');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

    public function updatecontentEquomPresencial()
    {
        $data  = Input::all();
        $datacontent = Input::only(['id','descripcion','content']);
        $rules = [
            'descripcion'    => 'required',
            'content'  => 'required',
        ];
        $contenido = $this->equompresencialcontentRepo->get($datacontent['id']);

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            $contenido->fill($datacontent);
            $contenido->save();
            return Redirect::route('content_equom_presencial');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

    /*
     *
     */

    public function imagesCompubox()
    {
        $images = $this->compuboximageRepo->get();
        return View::make('admin/images-compubox', compact("images"));
    }

    public function imagesEquom()
    {
        $images = $this->equomimageRepo->get();
        return View::make('admin/images-equom', compact("images"));
    }

    public function imagesEquompresencial()
    {
        $images = $this->equompresencialimageRepo->get();
        return View::make('admin/images-equom-presencial', compact("images"));
    }

    public function formimagesCompubox($id)
    {
        $image = $this->compuboximageRepo->get($id);
        return View::make('admin/editimageform', array("image" => $image, "tipo_contenido" => "Compubox", "route" => "update_image_compubox"));
    }

    public function formimagesEquom($id)
    {
        $image = $this->equomimageRepo->get($id);
        return View::make('admin/editimageform', array("image" => $image, "tipo_contenido" => "Equom", "route" => "update_image_equom"));
    }

    public function formimagesEquompresencial($id)
    {
        $image = $this->equompresencialimageRepo->get($id);
        return View::make('admin/editimageform', array("image" => $image, "tipo_contenido" => "Equom Prensencial", "route" => "update_image_equom_presencial"));
    }

    public function updateimageCompubox()
    {
        $data  = Input::all();
        $dataiamge = Input::only(['descripcion']);
        $rules = [
            'descripcion'   => 'required'
        ];
        $file = Input::file('file');
        $image = $this->compuboximageRepo->get($data['id']);

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            if ($file)
            {
                $file->move(public_path()."/img/", $image->url);
            }

            $image->fill($dataiamge);
            $image->save();
            return Redirect::route('images_compubox');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

    public function updateimageEquompresencial()
    {
        $data  = Input::all();
        $dataiamge = Input::only(['descripcion']);
        $rules = [
            'descripcion'   => 'required',
        ];
        $file = Input::file('file');

        $image = $this->equompresencialimageRepo->get($data['id']);

        $validation = \Validator::make($data, $rules);

        if($validation->passes())
        {
            if ($file)
            {
                $file->move(public_path()."/img/", $image->url);
            }
            $image->fill($dataiamge);
            $image->save();
            return Redirect::route('images_equompresencial');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

    public function updateimageEquom()
    {
        $data  = Input::all();
        $dataiamge = Input::only(['descripcion']);
        $rules = [
            'descripcion'   => 'required',
        ];
        $file = Input::file('file');

        $image = $this->equomimageRepo->get($data['id']);

        $validation = \Validator::make($data, $rules);

        if($validation->passes())
        {
            if ($file)
            {
                $file->move(public_path()."/img/", $image->url);
            }
            $image->fill($dataiamge);
            $image->save();
            return Redirect::route('images_equom');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }
}