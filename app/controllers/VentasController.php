<?php

use CompuboxStore\Repositories\VentasRepo;

class VentasController extends BaseController{

    protected $ventasRepo;

    public function __construct(VentasRepo $ventasRepo)
    {
        $this->ventasRepo = $ventasRepo;
    }

    public function index()
    {
        $ventas = $this->ventasRepo->getSummary();
        return View::make('admin/ventas', array('nameView' => 'Ventas |','ventas' => $ventas));
    }

    public function detalle($id)
    {
        $venta = $this->ventasRepo->get($id);
        return View::make('admin/detalleventa', compact('venta'));
    }

    public function verificarpago()
    {
        DB::beginTransaction();
        $data  = Input::all();
        $venta = $this->ventasRepo->get($data['id']);
        $venta->estado = 'pago_verificado';
        $venta->save();

        $fromEmail = 'carmanuel.zarate@gmail.com';
        $fromName = 'Carlos Zarate';
        try
        {
            Mail::send('emails.client.confirmacionpagomail', array('codigo' => $venta->codigo), function($message) use ($fromEmail, $fromName)
            {
                $message->to($fromEmail, $fromName);
                $message->from($fromEmail,$fromName);
                $message->subject('Pago Confirmado');
            });
            DB::commit();
        }catch (\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        return Response::json($data);
    }


} 