<?php

use CompuboxStore\Entities\User;
use CompuboxStore\Repositories\UserRepo;

class UserController extends BaseController{

    protected $userRepo;

    public function __construct(UserRepo $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function usuarios()
    {
        $users = $this->userRepo->get();
        return View::make('admin/users', compact('users'));
    }

    public function adduserform()
    {
        return View::make('admin/adduserform');
    }

    public function updateuserform($id)
    {
        $user = $this->userRepo->get($id);
        return View::make('admin/updateuserform', compact('user'));
    }

    public function updateperfilform()
    {
        $user = Auth::user();
        return View::make('admin/updateperfilform', compact('user'));
    }

    public function updateuser()
    {
        $data  = Input::all();
        $datauser = Input::only(['id','full_name','email','password','user_type']);
        $rules = [
            'full_name'    => 'required',
            'email'  => 'email|required',
        ];
        $user = $this->userRepo->get($datauser['id']);

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            $user->fill($datauser);
            $user->save();
            return Redirect::route('usuarios');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

    public function updateperfil()
    {
        $data  = Input::all();
        $datauser = Input::only(['full_name','email','password']);
        $rules = [
            'full_name'    => 'required',
            'email'  => 'email|required',
        ];
        $user = Auth::user();

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            $user->fill($datauser);
            $user->save();
            return Redirect::route('admin_home');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

    public function dropuser()
    {
        $data  = Input::all();
        $user = $this->userRepo->get($data['id']);
        $user->delete();
        return Response::json("ok");
    }

    public function register()
    {
        $data  = Input::all();
        $datauser = Input::only(['full_name','email','password','user_type']);
        $rules = [
            'full_name'    => 'required',
            'email'  => 'email|required',
            'password'  => 'required'
        ];

        $validation = \Validator::make($data, $rules);

        if($validation->passes()) {
            User::create($datauser);
            return Redirect::route('usuarios');
        }
        return Redirect::back()->withInput()->withErrors($validation);
    }

} 