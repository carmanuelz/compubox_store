<?php

/**
 * Login
 */
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);

/*
 * Logout
 */
Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

/*
 * CompuBoxStore
 */

Route::group(['before' => 'guest'], function(){
    /*
     * Signin
     */
    Route::get('signin', ['as' => 'signin', 'uses' => 'AuthController@index']);
});