<?php
Route::group(['before' => 'auth'], function(){
    /**
     * Home
     */
    Route::get('/', ['as' => 'admin_home', 'uses' => 'AdminController@index']);

    /*
     * Formularios para editar usuario autenticado
     */
    Route::get('updateperfilform', ['as' => 'updateperfilform', 'uses' => 'UserController@updateperfilform']);

    /*
     *Formulario Actualizar perfil
     */
    Route::put('updateperfil', ['as' => 'updateperfil', 'uses' => 'UserController@updateperfil']);

    Route::group(['before' => 'contenido'], function()
    {
        /**
         * Administrar Contenido EQUOM
         */
        Route::get('content_equom', ['as' => 'content_equom', 'uses' => 'AdminController@contentEquom']);

        /**
         * Administrar Contenido EQUOM Presencial
         */
        Route::get('content_equom_presencial', ['as' => 'content_equom_presencial', 'uses' => 'AdminController@contentEquomPresencial']);

        /**
         * Administrar Contenido CompuBox
         */
        Route::get('content_compubox', ['as' => 'content_compubox', 'uses' => 'AdminController@contentCompubox']);

        /**
         * Formulario de edicion de Contenido Compubox
         */
        Route::get('form_content_compubox/{id}', ['as' => 'form_content_compubox', 'uses' => 'AdminController@formcontentCompubox']);

        /**
         * Formulario de edicion de Contenido Equom
         */
        Route::get('form_content_equom/{id}', ['as' => 'form_content_equom', 'uses' => 'AdminController@formcontentEquom']);

        /**
         * Formulario de edicion de Contenido EquomPresencial
         */
        Route::get('form_content_equompresencial/{id}', ['as' => 'form_content_equompresencial', 'uses' => 'AdminController@formcontentEquomPresencial']);

        /*
         *
         */
        Route::put('update_content_compubox', ['as' => 'update_content_compubox', 'uses' => 'AdminController@updatecontentCompubox']);

        /*
         *
         */
        Route::put('update_content_equom', ['as' => 'update_content_equom', 'uses' => 'AdminController@updatecontentEquom']);

        /*
         *
         */
        Route::put('update_content_equompresencial', ['as' => 'update_content_equompresencial', 'uses' => 'AdminController@updatecontentEquomPresencial']);

        /*
         * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
         */

        /**
         * Administrar Imagenes CompuBox
         */
        Route::get('images_compubox', ['as' => 'images_compubox', 'uses' => 'AdminController@imagesCompubox']);

        /**
         * Administrar Imagenes Equom
         */
        Route::get('images_equom', ['as' => 'images_equom', 'uses' => 'AdminController@imagesEquom']);

        /**
         * Administrar Imagenes Equom Presencial
         */
        Route::get('images_equompresencial', ['as' => 'images_equompresencial', 'uses' => 'AdminController@imagesEquompresencial']);

        /*
         * Formulario para editar imagenes de Equom
         */
        Route::get('form_images_equom/{id}', ['as' => 'form_images_equom', 'uses' => 'AdminController@formimagesEquom']);

        /*
         * Formulario para editar imagenes de Compubox
         */
        Route::get('form_images_compubox/{id}', ['as' => 'form_images_compubox', 'uses' => 'AdminController@formimagesCompubox']);

        /*
         * Formulario para editar imagenes de Compubox
         */
        Route::get('form_images_equom_presencial/{id}', ['as' => 'form_images_equom_presencial', 'uses' => 'AdminController@formimagesEquompresencial']);

        /*
         *
         */
        Route::post('update_image_compubox', ['as' => 'update_image_compubox', 'uses' => 'AdminController@updateimageCompubox']);

        /*
         *
         */
        Route::post('update_image_equom_presencial', ['as' => 'update_image_equom_presencial', 'uses' => 'AdminController@updateimageEquompresencial']);

        /*
         *
         */
        Route::post('update_image_equom', ['as' => 'update_image_equom', 'uses' => 'AdminController@updateimageEquom']);

    });


    Route::group(['before' => 'isadmin'], function() {
        /*
         * Administracion de usuarios
         */
        Route::get('usuarios', ['as' => 'usuarios', 'uses' => 'UserController@usuarios']);

        /*
         * Formularios para registrar de usuarios
         */
        Route::get('adduserform', ['as' => 'adduserform', 'uses' => 'UserController@adduserform']);

        /*
         * Formularios para editar de usuarios
         */
        Route::get('updateuserform/{id}', ['as' => 'updateuserform', 'uses' => 'UserController@updateuserform']);

        /*
         * Actualizar usuario
         */
        Route::put('updateuser', ['as' => 'updateuser', 'uses' => 'UserController@updateuser']);

        /*
         * Eliminar de usuario
         */
        Route::post('drop_user', ['as' => 'drop_user', 'uses' => 'UserController@dropuser']);

        /*
         * Agregar un nuevo usuario
         */
        Route::post('register_user', ['as' => 'register_user', 'uses' => 'UserController@register']);

        /*
         * Administrar producto
         */
        Route::get('administrar_producto', ['as' => 'administrar_producto', 'uses' => 'ProductoController@administrar']);

        /*
         * Administrar producto
         */
        Route::put('update_producto', ['as' => 'update_producto', 'uses' => 'ProductoController@update']);
    });

    Route::group(['before' => 'ventas'], function() {
        /*
         * Ventas realizadas
         */
        Route::get('ventas', ['as' => 'ventas', 'uses' => 'VentasController@index']);

        /**
         * Verificar el pago
         */
        Route::post('verificar_pago', ['as' => 'verificar_pago', 'uses' => 'VentasController@verificarpago']);

        /*
         * Muestra los detalles de la venta (Ejm. direccion de envio)
         */
        Route::get('detalleventa/{id}', ['as' => 'detalleventa', 'uses' => 'VentasController@detalle']);
    });



});