<?php
/*
 * Formulario para realizar la compra
 */
Route::get('/comprar', ['as' => 'comprar', 'uses' => 'ComprarController@index']);

/*
 * Formulario para enviar los datos del pago mediante ticket
 */
Route::get('/ticket', ['as' => 'ticket', 'uses' => 'TicketController@index']);

/*
 * Controlador que verifica el pago y actualiza la venta
 */
Route::post('/validar_ticket', ['as' => 'validar_ticket', 'uses' => 'TicketController@validar']);

/*
 * Controlador que registra la venta, ticket, cliente y direccion de envio
 */
Route::post('/registrar_venta', ['as' => 'registrar_venta', 'uses' => 'ComprarController@registrar_venta']);

/*
 * Servicio para cargar los ubigeos para los combobox dinamicos
 */
Route::get('/ubigeos', ['as' => 'ubigeos', 'uses' => 'UbigeoController@getubigeos']);

/*
 * Enlace para verificar el correo de el comprador
 */
Route::get('/verificacion/{codigo}', ['as' => 'verificacion', 'uses' => 'ComprarController@verificacion']);

/**
 * Home Compubox Layout
 */
Route::get('/', ['as' => 'home_compubox', 'uses' => 'HomeCompuboxController@index']);

/**
 * Contact
 */
Route::get('/contacto', ['as' => 'contact', 'uses' => 'ContactController@index']);

/*
 * Controlador que envia el correo de contacto
 */
Route::post('/sendmssgcontact', ['as' => 'sendmssgcontact', 'uses' => 'ContactController@send']);
