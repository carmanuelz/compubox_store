<?php

namespace EquomPresencial\Repositories;

use EquomPresencial\Entities\Equompresencialimage;

class EquompresencialimageRepo extends BaseRepo{

    public function getModel()
    {
        return new Equompresencialimage();
    }
}