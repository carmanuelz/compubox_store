<?php

namespace EquomPresencial\Repositories;

use EquomPresencial\Entities\Equompresencialcontent;

class EquompresencialcontentRepo extends BaseRepo{

    public function getModel()
    {
        return new Equompresencialcontent();
    }
}