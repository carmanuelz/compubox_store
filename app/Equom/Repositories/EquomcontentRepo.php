<?php namespace Equom\Repositories;

use Equom\Entities\Equomcontent;

class EquomcontentRepo extends BaseRepo{

    public function getModel()
    {
        return new Equomcontent();
    }
}