<?php namespace Equom\Repositories;

use Equom\Entities\Equomimage;

class EquomimageRepo extends BaseRepo{

    public function getModel()
    {
        return new Equomimage();
    }
}