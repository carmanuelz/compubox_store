<?php

namespace CompuboxStore\Managers;


class RegisterManager extends ManagerBase{

    public function getRules()
    {
        $rules = [
            'nombre'    => 'required',
            'apellido'  => 'required',
            'telefono'  => 'required|numeric',
            'dni'       => 'required|numeric',
            'email'     => 'required|email|confirmed',
            'email_confirmation' => 'required',
            'direccion' => 'required'
        ];

        return $rules;
    }
}