<?php

namespace CompuboxStore\Repositories;

use CompuboxStore\Entities\Venta;


class VentasRepo extends BaseRepo{

    public function getModel()
    {
        return new Venta();
    }

    public function getSummary()
    {
        return Venta::join('clientes', 'clientes.id', '=', 'ventas.cliente_id')
            ->join('direccionenvios', 'direccionenvios.id', '=', 'ventas.direccionenvio_id')
            ->join('ubigeos as prov', 'prov.id', '=','direccionenvios.ubigeo_id')
            ->join('ubigeos as dpt', 'dpt.id', '=', 'prov.depend')
            ->join('tickets', 'tickets.codigo', '=', 'ventas.codigo')
            ->get(['ventas.id', 'clientes.nombre', 'clientes.apellido', 'clientes.dni', 'ventas.total', 'ventas.tipo', 'ventas.codigo', 'ventas.fecha','ventas.estado','direccionenvios.direccion', 'prov.descripcion as provincia','dpt.descripcion as departamento','tickets.nro_operacion','tickets.fecha_confirmacion']);
    }

    public function getByCode($codigo)
    {
        return Venta::where('codigo','=',$codigo)->first();
    }
}