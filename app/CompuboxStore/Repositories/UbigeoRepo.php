<?php
namespace CompuboxStore\Repositories;

use CompuboxStore\Entities\Ubigeo;

class UbigeoRepo extends BaseRepo{

    public function getModel()
    {
        return new Ubigeo();
    }
}