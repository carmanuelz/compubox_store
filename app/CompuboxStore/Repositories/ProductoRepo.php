<?php

namespace CompuboxStore\Repositories;
use CompuboxStore\Entities\Producto;

class ProductoRepo extends BaseRepo{

    public function getModel()
    {
        return new Producto();
    }

    public function getActive()
    {
        return Producto::where('estado', '=', true)->first();
    }
}