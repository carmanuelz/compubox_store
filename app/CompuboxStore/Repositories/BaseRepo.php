<?php
namespace CompuboxStore\Repositories;

abstract class  BaseRepo {

    protected $model;

    public  function __construct()
    {
        $this->model = $this->getModel();
    }

    abstract public function getModel();

    public function get($id = false)
    {
        if($id === false)
            return $this->model->get();
        else
            return $this->model->find($id);
    }
} 