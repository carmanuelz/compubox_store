<?php

namespace CompuboxStore\Repositories;

use CompuboxStore\Entities\User;

class UserRepo extends BaseRepo{

    public function getModel()
    {
        return new User();
    }
}