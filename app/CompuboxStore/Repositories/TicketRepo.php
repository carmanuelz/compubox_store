<?php
namespace CompuboxStore\Repositories;

use CompuboxStore\Entities\Ticket;

class TicketRepo extends BaseRepo{

    public function getModel()
    {
        return new Ticket();
    }

    public function getByCode($codigo)
    {
        return Ticket::where('codigo','=',$codigo)->first();
    }
}