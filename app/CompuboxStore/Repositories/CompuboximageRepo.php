<?php namespace CompuboxStore\Repositories;

use CompuboxStore\Entities\Compuboximage;

class CompuboximageRepo extends  BaseRepo{

    public function getModel()
    {
        return new Compuboximage();
    }
}