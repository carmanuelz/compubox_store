<?php namespace CompuboxStore\Repositories;

use CompuboxStore\Entities\Compuboxstorecontent;

class CompuboxstorecontentRepo extends  BaseRepo{

    public function getModel()
    {
        return new Compuboxstorecontent();
    }
}