<?php namespace CompuboxStore\Entities;

class Direccionenvio extends \Eloquent {
	protected $fillable = ['direccion','ubigeo_id'];

    public function ubigeo()
    {
        return $this->hasOne('CompuboxStore\Entities\Ubigeo', 'id', 'ubigeo_id');
    }
}