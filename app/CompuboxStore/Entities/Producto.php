<?php namespace CompuboxStore\Entities;

class Producto extends \Eloquent {
	protected $fillable = ['descripcion','so','almacenamiento', 'memorias', 'cpu', 'gpu', 'otros'];
}