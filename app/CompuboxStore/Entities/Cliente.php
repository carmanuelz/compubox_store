<?php namespace CompuboxStore\Entities;

class Cliente extends \Eloquent {
	protected $fillable = ['nombre','apellido','telefono','dni','email'];
}