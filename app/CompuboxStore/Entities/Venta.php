<?php namespace CompuboxStore\Entities;

class Venta extends \Eloquent {
	protected $fillable = ['nombre', 'apellido', 'dni', 'telefono', 'email', ];

    public function ventaproducto()
    {
        return $this->hasMany('CompuboxStore\Entities\Ventaproducto');
    }

    public function cliente()
    {
        return $this->hasOne('CompuboxStore\Entities\Cliente', 'id', 'cliente_id');
    }

    public function  direccionenvio()
    {
        return $this->hasOne('CompuboxStore\Entities\Direccionenvio', 'id', 'direccionenvio_id');
    }

    public function getVentaEstadoAttribute()
    {
        return \Lang::get('utils.venta_estados.'.$this->estado);
    }

    public function getVentaEstadoLabelAttribute()
    {
        return \Lang::get('utils.venta_estados_labels.'.$this->estado);
    }
}