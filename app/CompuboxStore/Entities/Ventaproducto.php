<?php namespace CompuboxStore\Entities;

class Ventaproducto extends \Eloquent {
	protected $fillable = ['producto_id', 'venta_id', 'precio_unitario', 'total_producto', 'cantidad'];
}