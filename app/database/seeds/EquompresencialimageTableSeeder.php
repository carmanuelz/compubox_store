<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use EquomPresencial\Entities\Equompresencialimage;

class EquompresencialimageTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
            Equompresencialimage::create([
                "code"  => "imagenequompres".$index,
                "url"   => "imgequompres".$index.".jpg",
                "descripcion" =>"nueva imagen"
			]);
		}
	}

}