<?php

use Faker\Factory as Faker;
use CompuboxStore\Entities\Cliente;
use CompuboxStore\Entities\Direccionenvio;
use CompuboxStore\Entities\Venta;
use CompuboxStore\Entities\Ventaproducto;
use CompuboxStore\Entities\Ticket;

class VentaTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 200) as $index)
		{
            $cliente = Cliente::create([
                'nombre'    => $faker->firstName,
                'apellido'  => $faker->lastName,
                'telefono'  => $faker->phoneNumber,
                'dni'       => $faker->numerify($string = '########'),
                'email'     => $faker->email
            ]);

            $direccionenvio = Direccionenvio::create([
                'direccion' =>  $faker->streetAddress,
                'ubigeo_id' =>  $faker->numberBetween($min = 26, $max = 220)
            ]);

			$venta = Venta::create([
                'fecha'             => $faker->dateTimeBetween($startDate = '-60 days', $endDate = 'now'),
                'total'             => 300.00,
                'estado'            => 'mail_no_verificado',
                'tipo'              => 'ticket',
                'codigo'            => $faker->lexify($string = '????????'),
                'cliente_id'        => $cliente->id,
                'direccionenvio_id' => $direccionenvio->id
			]);

            Ventaproducto::create([
                'cantidad'          => 1,
                'precio_unitario'   => 300,
                'venta_id'          => $venta->id,
                'producto_id'       => 1
            ]);

            Ticket::create([
                'codigo'            => $venta->codigo
            ]);
		}
	}

}