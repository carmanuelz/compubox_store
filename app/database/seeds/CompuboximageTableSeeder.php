<?php

use Faker\Factory as Faker;
use CompuboxStore\Entities\Compuboximage;

class CompuboximageTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 5) as $index)
		{
			Compuboximage::create([
                "code"  => "imagencompubox".$index,
                "url"   => "imgcompubox".$index.".jpg",
                "descripcion" =>"nueva imagen"
			]);
		}
	}

}