<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Equom\Entities\Equomcontent;

class EquomcontentTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Equomcontent::create([
                "code" => "code".$index,
                "descripcion" => "content".$index,
                "content" => $faker->text($maxNbChars = 200)
			]);
		}
	}

}