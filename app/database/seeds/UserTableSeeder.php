<?php

use CompuboxStore\Entities\User;

class UserTableSeeder extends Seeder {

	public function run()
	{
		User::create([
            'full_name'  => 'Carlos Zarate',
            'email'     => 'carmanuel.zarate@gmail.com',
            'password'  => '123456',
            'user_type' => 'master'
        ]);
	}

}