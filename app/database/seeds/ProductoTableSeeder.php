<?php

use CompuboxStore\Entities\Producto;

class ProductoTableSeeder extends Seeder {

	public function run()
	{
		Producto::create([
            'id'            => 1,
            'descripcion'   => 'AndroidTV4203GBSRAM16GBSD',
            'so'            => 'Android 4.3.0',
            'almacenamiento'=> '16 GB',
            'memorias'      => '3 GB',
            'cpu'           => '1.6 GHz',
            'gpu'           => 'Mali 400',
            'otros'         => '',
            'estado'        => true,
            'precio_venta'  => 300
        ]);
	}

}