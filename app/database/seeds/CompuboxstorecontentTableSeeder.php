<?php
use Faker\Factory as Faker;
use CompuboxStore\Entities\Compuboxstorecontent;

class CompuboxstorecontentTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 3) as $index)
		{
			Compuboxstorecontent::create([
                "code" => "code".$index,
                "descripcion" => "content".$index,
                "content" => $faker->text($maxNbChars = 200)
			]);
		}
	}

}