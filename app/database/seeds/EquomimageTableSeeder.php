<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use Equom\Entities\Equomimage;

class EquomimageTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Equomimage::create([
                "code"  => "imagenequom".$index,
                "url"   => "imgequom".$index.".jpg",
                "descripcion" =>"nueva imagen"
			]);
		}
	}

}