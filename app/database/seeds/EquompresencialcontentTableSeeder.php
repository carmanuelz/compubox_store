<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use EquomPresencial\Entities\Equompresencialcontent;

class EquompresencialcontentTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Equompresencialcontent::create([
                "code" => "code".$index,
                "descripcion" => "content".$index,
                "content" => $faker->text($maxNbChars = 200)
			]);
		}
	}

}