<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('CompuboximageTableSeeder');
        $this->call('EquomimageTableSeeder');
        $this->call('EquompresencialimageTableSeeder');
        $this->call('EquompresencialcontentTableSeeder');
        $this->call('EquomcontentTableSeeder');
        $this->call('CompuboxstorecontentTableSeeder');
		$this->call('ProductoTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('UbigeoTableSeeder');
		$this->call('VentaTableSeeder');
	}

}
