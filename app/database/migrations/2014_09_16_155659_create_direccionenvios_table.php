<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDireccionenviosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('direccionenvios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('direccion');
            $table->integer('ubigeo_id')->unsigned();
            $table->foreign('ubigeo_id')->references('id')->on('ubigeos');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('direccionenvios');
	}

}
