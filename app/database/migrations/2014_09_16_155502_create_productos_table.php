<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productos', function(Blueprint $table)
		{
            $table->increments('id');
            $table->text('descripcion');
            $table->string('so');
            $table->string('almacenamiento');
            $table->string('memorias');
            $table->string('cpu');
            $table->string('gpu');
            $table->text('otros');
            $table->boolean('estado');
            $table->float('precio_venta');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productos');
	}

}
