<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVentasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ventas', function(Blueprint $table)
		{
            $table->increments('id');
            $table->date('fecha');
            $table->float('total');
            $table->enum('estado',['mail_no_verificado', 'mail_verificado', 'pago_no_verificado', 'pago_verificado']);
            $table->enum('tipo',['paypal','ticket']);
            $table->string('codigo');
            $table->integer('cliente_id')->unsigned();
            $table->integer('direccionenvio_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreign('direccionenvio_id')->references('id')->on('direccionenvios');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ventas');
	}

}
