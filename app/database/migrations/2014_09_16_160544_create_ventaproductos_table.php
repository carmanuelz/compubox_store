<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVentaproductosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ventaproductos', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('cantidad');
            $table->float('precio_unitario');
            $table->integer('venta_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->foreign('producto_id')->references('id')->on('productos');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ventaproductos');
	}

}
