<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUbigeosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ubigeos', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('dpt');
            $table->integer('prov');
            $table->integer('depend')->nullable();
            $table->timestamps();
        });

        /*Schema::table('ubigeos', function($table) {
            $table->foreign('depend')->references('id')->on('ubigeos')->onDelete('no action')->onUpdate('no action');
        });*/
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ubigeos');
	}

}
