<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * HOME
 */

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::group(['prefix' => 'admin'], function() {
    require (__DIR__.'/routes/compubox_store/admin.php');
});

Route::group(['prefix' => 'auth'], function() {
    require (__DIR__.'/routes/compubox_store/auth.php');
});

Route::group(['prefix' => 'compubox_store'], function() {
    require (__DIR__.'/routes/compubox_store/client.php');
});




