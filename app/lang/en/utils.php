<?php
return[
    'venta_estados' => [
        'mail_no_verificado'    => 'Mail no verificado',
        'mail_verificado'       => 'Mail Verificado',
        'pago_no_verificado'    => 'Pago no verificado',
        'pago_verificado'       => 'Pago verificado'
    ],
    'venta_estados_labels' => [
        'mail_no_verificado'    => '<strong>Mail no verificado</strong>',
        'mail_verificado'       => '<strong>Mail verificado</strong>',
        'pago_verificado'       => '<strong>Pago Verificado</strong>',
        'pago_no_verificado'    => '<strong>Pago no verificado</strong>',
    ]
];