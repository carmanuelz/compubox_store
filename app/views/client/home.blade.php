<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>EQUOM</title>
		<meta name="description" content="" />
		<meta name="author" content="Spinity Tech" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/home/plus.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/home/component.css')}}" />
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container-fluid demo-1">
			<div id="large-header" class="large-header">
				<canvas id="demo-canvas"></canvas>
				<div id="content">
					<div id="logo">
						<div class="row">
							<div class="col-md-4 col-md-offset-4">
								<img src="{{asset('img/logo-EQUOM.svg')}}" alt="Logo EQUOM"/>
							</div>
						</div>
					</div>
					<hr/>
					<div id="sections">
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="col-md-4">
									<p class="title">Relaciones <br/>De Poder</p>
									<img src="{{asset('img/icon-relaciones-poder.svg')}}" alt=""/>
								</div>
								<div class="col-md-4">
									<p class="title">CompuBox</p>
									<a href="{{ route('home_compubox') }}"><img class="cb" src="{{asset('img/icon-compubox.svg')}}" alt=""/></a>
								</div>
								<div class="col-md-4">
									<p class="title">Energías <br/>Renovables</p>
									<img src="{{asset('img/icon-energias-renovables.svg')}}" alt=""/>
								</div>
							</div>
						</div>
						<div class="transparency row">
							<div class="col-md-6 col-md-offset-3">
								<div class="col-md-4">
									<p class="more p-rp"><a href="" data-toggle="modal" data-target="#rel_poder">Leer más <span class="icon-plus-alt"></span></a></p>
								</div>
								<div class="col-md-4">
									<p class="more p-cb"><a href="{{ route('home_compubox') }}">Leer más <span class="icon-plus-alt"></span></a></p>
								</div>
								<div class="col-md-4">
									<p class="more p-er"><a href="" data-toggle="modal" data-target="#ene_ren">Leer más <span class="icon-plus-alt"></span></a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /container -->

		<!-- Relaciones de Poder -->
		<div class="modal fade right" id="rel_poder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<p>
							Es el consumidor quien debería ejercer un poder más funcional y dinámico dentro de todo el engranaje económico, pues es quien asume los beneficios y consecuencias de las decisiones de compra en relación al entorno natural y social, y quien desconoce el verdadero poder que puede ejercer sobre el mercado, debido a la desorganización. ¿Cómo gestionar un consumo más eficiente?, cambiando las relaciones de poder en el mercado.
						</p>
					</div>
				</div>
			</div>
		</div>

		<!-- Relaciones de Poder -->
		<div class="modal fade left" id="ene_ren" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<p>
							El cambio climático es uno de los problemas más urgentes de nuestra era, un cambio en la matriz energética contribuiría a la integración del hombre con la naturaleza, permitiendo la conservación tanto de su especie como del entorno del cual dispone. La producción y el consumo inexorablemente tendrán que adaptarse a nuevas fuentes de energía pues es una condición esencial en el desarrollo evolutivo de nuestra especie. El desarrollo de tecnologías que usan como fuente las energías renovables es un contexto actual, que comenzará su adaptación comercial en las próximas décadas.
						</p>
					</div>
				</div>
			</div>
		</div>

		<script src="{{ asset('js/jquery-2.1.1.js')}}"></script>
		<script src="{{ asset('js/bootstrap.min.js')}}"></script>
		<script src="{{ asset('js/home/TweenLite.min.js')}}"></script>
		<script src="{{ asset('js/home/EasePack.min.js')}}"></script>
		<script src="{{ asset('js/home/rAF.js')}}"></script>
		<script src="{{ asset('js/home/home.js')}}"></script>
	</body>
</html>