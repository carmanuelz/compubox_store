@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>Contáctanos</h1>
            <div class="row">
                <div class="col-lg-6">
                    {{ Form::open(['route'=>'sendmssgcontact', 'method' => 'POST']) }}
                        <div class="form-group">
                            {{ Form::label('name', 'NOMBRE') }}
                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                            {{ $errors->first('name','<span>:message</span>') }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('email', 'CORREO') }}
                            {{ Form::text('email', null, ['class' => 'form-control']) }}
                            {{ $errors->first('email','<span>:message</span>') }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('subject', 'ASUNTO') }}
                            {{ Form::text('subject', null, ['class' => 'form-control']) }}
                            {{ $errors->first('subject','<span>:message</span>') }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('message', 'MENSAJE') }}
                            {{ Form::textarea('message', null, ['class' => 'form-control']) }}
                            {{ $errors->first('message','<span>:message</span>') }}
                        </div>
                         <div class="form-group">
                            <button type="submit" class="btn btn-material-teal withripple">Enviar</button>
                        </div>
                    {{ Form::close() }}
                </div>
                <div class="col-lg-6">

                </div>
            </div>
        </div>
    </div>
</div>
@stop