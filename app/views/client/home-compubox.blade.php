@extends('layout')

@section('content')
<div id="contenido" class="container">
	<div class="row">
		<h1 class="text-center">Compu Box</h1>
	</div>
	<div id="intro" class="row">
		<div class="col-lg-2 buttons">
			<button class="sphere">$ Price</button>
		</div>
		<div class="col-lg-8">
			<figure>
				<img src="{{ asset('imgs-test/intro-image.jpg')}}" alt="">
			</figure>
		</div>
		<div class="col-lg-2 buttons">
			<a href="{{ route('comprar')}}" class="sphere">Comprar</a>
		</div>
	</div>
	<div id="caracteristicas" class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<figure>
				<img src="{{ asset('imgs-test/caracteristicas-image.jpg')}}" alt="">
			</figure>
		</div>
	</div>
	<div id="beneficios" class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<figure>
				<img src="{{ asset('imgs-test/beneficios-image.jpg')}}" alt="">
			</figure>
		</div>
	</div>
	<div id="video" class="row">
		
	</div>
	<div id="gallery" class="row">
		<div class="col-lg-2 buttons">
			<button class="sphere">Price</button>
		</div>
		<div class="col-lg-8">
			<figure>
				<img src="{{ asset('imgs-test/intro-image.jpg')}}" alt="">
			</figure>
		</div>
		<div class="col-lg-2 buttons">
			<a href="{{ route('comprar')}}" class="sphere">Comprar</a>
		</div>
	</div>
	<div id="testimonios" class="row">
		<div class="col-lg-8 col-lg-offset-2 columns text-center">
			<h2>¿Qué piensan de Compu Box?</h2>
		</div>
		<div class="col-lg-8 col-lg-offset-2 columns">
            <div class="cd-testimonials-wrapper cd-container">
                <ul class="cd-testimonials">
                    <li>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <div class="cd-author">
                            <ul class="cd-author-info">
                                <li>Nombre Apellido</li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ea, perferendis error repudiandae numquam dolor fuga temporibus. Unde omnis, consequuntur.</p>
                        <div class="cd-author">
                            <ul class="cd-author-info">
                                <li>Nombre Apellido</li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam totam nulla est, illo molestiae maxime officiis, quae ad, ipsum vitae deserunt molestias eius alias.</p>
                        <div class="cd-author">
                            <ul class="cd-author-info">
                                <li>Nombre Apellido</li>
                            </ul>
                        </div>
                    </li>

                </ul> <!-- cd-testimonials -->
            </div> <!-- cd-testimonials-wrapper -->
        </div>
	</div>
</div>
@stop

@section('js-content')
	<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.flexslider-min.js')}}"></script>
	<script type="text/javascript" language="javascript" src="{{ asset('js/testimonials.js')}}"></script>
@stop