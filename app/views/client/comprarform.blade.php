@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 columns">
            <h1>Un poco de ti...</h1>
            <div class="well bs-component">
                {{ Form::open(['route'=>'registrar_venta', 'method' => 'POST']) }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('nombre', 'Nombres (*)') }}
                                {{ Form::text('nombre', null, ['placeholder' => 'Ingresa tu Nombre', 'class' => 'form-control']) }}
                                {{ $errors->first('nombre','<small class="error">:message</small>') }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('apellido', 'Apellidos (*)') }}
                                {{ Form::text('apellido', null, ['placeholder' => 'Ingresa tus Apellidos', 'class' => 'form-control']) }}
                                {{ $errors->first('apellido','<small class="error">:message</small>') }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('telefono', 'Teléfono (*)') }}
                                {{ Form::text('telefono', null, ['placeholder' => 'Ingresa tu teléfono', 'class' => 'form-control']) }}
                                {{ $errors->first('telefono','<small class="error">:message</small>') }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('dni', 'DNI (*)') }}
                                {{ Form::text('dni', null, ['placeholder' => 'Ingresa tu DNI', 'class' => 'form-control']) }}
                                {{ $errors->first('dni','<small class="error">:message</small>') }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('email', 'Correo Electrónico (*)') }}
                                {{ Form::text('email', null, ['placeholder' => 'Ingresa tu correo electrónico', 'class' => 'form-control']) }}
                                {{ $errors->first('email','<small class="error">:message</small>') }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('email_confirmation', 'Confirmar tu correo (*)') }}
                                {{ Form::text('email_confirmation', null, ['placeholder' => 'Vuelve a escribir tu correo', 'class' => 'form-control']) }}
                                {{ $errors->first('email_confirmation','<small class="error">:message</small>') }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('dpt', 'Departamento (*)') }}
                                {{ Form::select('dpt', array(), null, array('class' => 'form-control')); }}
                                {{ $errors->first('dpt','<small class="error">:message</small>') }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('ubigeo_id', 'Provincia (*)') }}
                                {{ Form::select('ubigeo_id', array(), null, array('class' => 'form-control')); }}
                                {{ $errors->first('ubigeo_id','<small class="error">:message</small>') }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                {{ Form::label('direccion', 'Dirección de Envío (*)') }}
                                {{ Form::text('direccion', null, ['placeholder' => 'Ingresa la dirección donde llegará CompuBox', 'class' => 'form-control']) }}
                                {{ $errors->first('direccion','<small class="error">:message</small>') }}
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <button class="btn btn-material-teal withripple">Continuar</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop
@section('js-content')
<script type="text/javascript" charset="utf-8">

    $(document).ready(function() {
         var arrayubigeos = getAjaxObject('{{ route( 'ubigeos') }}');
         cargarUbigeo(arrayubigeos,'ubigeo_id', 'dpt');
    } );
</script>
@stop
