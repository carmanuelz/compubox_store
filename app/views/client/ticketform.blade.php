@extends('layout')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css')}}">
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>Un poco de ti...</h1>
            {{ Form::open(['route'=>'validar_ticket', 'method' => 'POST']) }}
                <div class="row">
                    <div class="col-lg-6">
                    @if ($errors->first('codigo') != null)
                        <div class="form-group has-error">
                    @else
                        <div class="form-group">
                    @endif
                            {{ Form::label('codigo', 'Código (*)') }}
                            {{ Form::text('codigo', null, ['placeholder' => 'Ingresa tu Código de compra', 'class' => 'form-control']) }}
                            {{ $errors->first('codigo','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="col-lg-6">
                    @if ($errors->first('codigo') != null)
                        <div class="form-group has-error">
                    @else
                        <div class="form-group">
                    @endif
                            {{ Form::label('nro_operacion', 'Nro Operación (*)') }}
                            {{ Form::text('nro_operacion', null, ['placeholder' => 'Ingresa tu Código de Operación', 'class' => 'form-control']) }}
                            {{ $errors->first('nro_operacion','<small class="error">:message</small>') }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {{ Form::label('fecha_confirmacion', 'Fecha y Hora (*)') }}
                            {{ Form::text('fecha_confirmacion', null, ['class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD HH:mm', 'readonly']) }}

                            {{ $errors->first('fecha_confirmacion','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="col-lg-6">
                    @if ($errors->first('monto') != null)
                        <div class="form-group has-error">
                    @else
                        <div class="form-group">
                    @endif
                            {{ Form::label('monto', 'Monto (*)') }}
                            {{ Form::text('monto', null, ['placeholder' => 'Ingresa el monto de tu compra', 'class' => 'form-control']) }}
                            {{ $errors->first('monto','<small class="error">:message</small>') }}
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <button class="btn btn-material-teal withripple">Continuar</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

@section('js-content')
    <script type="text/javascript" language="javascript" src="{{ asset('js/moment.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/es.js')}}"></script>
<script type="text/javascript" charset="utf-8">

    $(document).ready(function() {

        var date = new Date();
        date.setDate(date.getDate());

        $('#fecha_confirmacion').datetimepicker({
            language:'es',
            maxDate: date
        });
    } );
</script>
@stop
