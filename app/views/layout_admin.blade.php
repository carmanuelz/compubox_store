<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>CompuBox</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Orbitron:700,400' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-wfont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ripples.css')}}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="navbar navbar-material-teal">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                       <span class="icon-bar"></span>
                       <span class="icon-bar"></span>
                       <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('admin_home')}}">Admin</a>
                </div>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav navbar-right">
                       <li><a href="{{ route('home')}}">Volver al sitio</a></li>
                       <li><a href="{{ route('usuarios') }}">Administrar Usuarios</a></li>
                       <li><a href="{{ route('updateperfilform') }}">Perfil</a></li>
                       <li><a href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@yield('content')

    <script src="{{ asset('js/vendor/modernizr.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jquery-2.1.1.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/material.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/ripples.js')}}"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10-dev/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/functiongen.js')}}"></script>

    <script type="text/javascript">
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
        });
    </script>
    @yield('js-content')
</body>
</html>