<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{ $nameView }}CompuBox</title>
    
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Orbitron:700,400' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-wfont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ripples.css')}}">

    @yield('css')

    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css')}}" />
</head>
<body>
    <div id="navbar">
        <div class="navbar navbar-material-white navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('home_compubox')}}"><img src="{{asset('img/compubox-logo.png')}}" alt="CompuBox"/></a>
                </div>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="scroll" href="#caracteristicas">Características</a></li>
                        <li><a class="scroll" href="{{ route('home_compubox')}}#beneficios">Beneficios</a></li>
                        <li><a class="scroll" href="{{ route('home_compubox')}}#video">Videos</a></li>
                        <li><a href="{{ route('ticket')}}">Confirmación</a></li>
                        <li><a href="{{ route('contact')}}">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


@yield('content')

    <footer class="footer">
        <div class="navbar navbar-material-white navbar-fixed-bottom" role="navigation">
            <div class="container">
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav copyright">
                        <li>2014 EQUOM - Todos los derechos reservados.</li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right rs">
                        <li><a class="fb" href="http://fb.com/compubox" target="_blank"></a></li>
                        <li><a class="tw" href="http://www.twitter.com/compubox"></a></li>
                    </ul>
                </div>
            </div>
        </div>
     </footer>

    <script src="{{ asset('js/vendor/modernizr.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jquery-2.1.1.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/functiongen.js')}}"></script>
    
    <script type="text/javascript">
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({
                scrollTop:$(this.hash).offset().top
            },1200);
        });
    </script>
    @yield('js-content')
</body>
</html>