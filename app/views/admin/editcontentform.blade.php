@extends('layout_admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Administrar Contenido {{ $tipo_contenido }}</h1>
            </div>
            {{ Form::model($contenido,['route'=>$route, 'method' => 'PUT', 'class' => 'form-horizontal']) }}
                {{ Form::hidden('id', $contenido->id) }}
                <div class="form-group">
                    {{ Form::label('descripcion', 'Descripcion', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('descripcion', null, ['class' => 'form-control']) }}
                        {{ $errors->first('descripcion','<span>:message</span>') }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('content', 'content', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('content', null, ['class' => 'form-control']) }}
                        {{ $errors->first('content','<span>:message</span>') }}
                    </div>
                </div>
                <div class="col-lg-10 col-lg-offset-2">
                    <a href="{{ URL::previous()}}" class="btn btn-default withripple">Cancelar</a>
                    <button type="submit" class="btn btn-primary withripple">Guardar</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop