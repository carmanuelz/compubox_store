@extends('layout_admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Administrar Contenido EQUOM Home</h1>
            </div>
            {{ Form::open(['route'=>'sendmssgcontact', 'method' => 'POST', 'class' => 'form-horizontal']) }}
                @foreach($images as $image)
                    <div class="form-group">
                        {{ Form::label('descripcion', $image->descripcion, ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-8">
                            <div class="content">
                                <p><img src="{{ asset('img')."/".$image->url }}" alt=""/></p>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <a href="{{ route('form_images_equom',$image->id) }}" class="btn btn-primary withripple">Editar</a>
                        </div>
                    </div>
                @endforeach
                <div class="col-lg-10 col-lg-offset-2">
                    <a href="{{ route('admin_home')}}" class="btn btn-default withripple">Volver atrás</a>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop