@extends('layout')

@section('content')
<div class="row">
    <div class="large-12 columns">
    <h1>Un poco de ti...</h1>
        <div class="row">
            <div class="large-6 columns">
                <label>Nombres (*)</label>
                <input name="name" type="text" placeholder="Ingresa tu Nombre" />
            </div>
            <div class="large-6 columns">
                <label>Apellidos (*)</label>
                <input name="last-name" type="text" placeholder="Ingresa tus Apellidos" />
            </div>
        </div>
    </div>
</div>
@stop