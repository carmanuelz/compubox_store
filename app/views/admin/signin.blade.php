<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign in | CompuBox</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Orbitron:700,400' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-wfont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ripples.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css')}}" />
</head>
<body>
    <div id="signin" class="container">
        <div class="row">
            <div class="col-lg-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ Form::open(['route'=>'login', 'method' => 'POST', 'class'=>'form-horizontal']) }}
                            <fieldset>
                                <legend>Iniciar Sesión</legend>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        {{ Form::label('email', 'Correo', array('class' => 'col-lg-2 control-label')) }}
                                        <div class="col-lg-10">
                                            {{ Form::text('email', null, array('class' => 'form-control')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        {{ Form::label('password', 'Contraseña', array('class' => 'col-lg-2 control-label')) }}
                                        <div class="col-lg-10">
                                            {{ Form::password('password', array('class' => 'form-control')) }}
                                            <div class="checkbox">
                                                <label>
                                                    {{ Form::checkbox('remember', null) }}
                                                    Recordarme
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <button type="submit" class="btn btn-primary">Entrar</button>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/vendor/modernizr.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jquery-2.1.1.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/material.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/ripples.js')}}"></script>
</body>
</html>