@extends('layout_admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Administrar Imagem {{ $tipo_contenido }}</h1>
            </div>
            {{ Form::model($image,['route'=>$route, 'method' => 'POST', 'class' => 'form-horizontal', 'files' => true]) }}
                {{ Form::hidden('id', $image->id) }}
                <div class="form-group">
                    {{ Form::label('descripcion', 'Descripcion', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('descripcion', null, ['class' => 'form-control']) }}
                        {{ $errors->first('descripcion','<span>:message</span>') }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('file', 'File', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::file('file', '', ['class' => 'form-control']) }}
                        {{ $errors->first('file','<span>:message</span>') }}
                    </div>
                </div>
                <div class="col-lg-10 col-lg-offset-2">
                    <a href="{{ URL::previous()}}" class="btn btn-default withripple">Cancelar</a>
                    <button type="submit" class="btn btn-primary withripple">Guardar</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop