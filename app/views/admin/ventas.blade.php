@extends('layout_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 columns">
            <div class="page-header">
                <h1>Administrar Ventas CompuBox</h1>
            </div>
            <table id="ventas">
                <thead>
                <tr>
                    <th width="11%">Fecha Compra</th>
                    <th width="11%">Provincia</th>
                    <th width="11%">Codigo</th>
                    <th width="11%">Fecha Pago</th>
                    <th width="11%">Nro Operacion</th>
                    <th width="11%">Monto</th>
                    <th width="11%">Estado</th>
                    <th width="11%">Verificar</th>
                    <th width="11%">Detalle</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ventas as $venta)
                    <tr>
                        <td>{{ $venta->fecha }}</td>
                        <td>{{ $venta->provincia }}</td>
                        <td>{{ $venta->codigo }}</td>
                        <td>{{ $venta->fecha_confirmacion }}</td>
                        <td>{{$venta->nro_operacion }}</td>
                        <td>{{ $venta->total }}</td>
                        <td>{{ $venta->venta_estado_label }}</td>
                        <td><a href="javascript:void(0)" onclick="verificarpago({{$venta->id}});" class="btn btn-success btn-xs withripple">Verificar</a></td>
                        <td><a href="{{ route('detalleventa', [$venta->id]) }}" class="btn btn-primary btn-xs withripple">Detalle</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('js-content')
<script type="text/javascript" charset="utf-8">

    function verificarpago(id)
    {
    $.ajax({
            url: "{{route('verificar_pago')}}",
            async: false,
            data: {'id':id},
            type: "POST"
        }).done(function(data)
        {
            location.reload();
        });
    }
    $(document).ready(function() {
        $('#ventas').dataTable(
        {
        "order": [[ 3, "desc" ]],
        columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [0, 1 ]
                }],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/380cb78f450/i18n/Spanish.json"
        }
        }
        );
    } );
</script>
@stop