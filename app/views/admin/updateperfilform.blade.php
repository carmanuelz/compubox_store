@extends('layout_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 columns">
            {{ Form::model($user, ['route'=>'updateperfil', 'method' => 'PUT', 'class' => 'form-horizontal']) }}
                <fieldset>
                    <legend>Actualizar Perfil</legend>
                    <div class="form-group">
                        {{ Form::label('full_name', 'Nombre Completo', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('full_name', null,['class' => 'form-control']) }}
                            {{ $errors->first('full_name','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'Correo', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('email', null,['class' => 'form-control']) }}
                            {{ $errors->first('email','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('password', 'Contraseña', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::password('password', ['class' => 'form-control']) }}
                            {{ $errors->first('password','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <a href="{{ route('admin_home')}}" class="btn btn-default withripple">Cancelar</a>
                        <button class="btn btn-material-teal withripple">Actualizar</button>
                    </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop