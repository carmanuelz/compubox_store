@extends('layout_admin')

@section('content')

<div class="container">
    <div class="row text-center">
        <div class="col-lg-4">
            <div class="btn-group" style="width: 100%;">
                <a href="javascript:void(0)" class="btn btn-material-indigo btn-lg withripple dropdown-toggle" data-toggle="dropdown" title="Administrar Contenido" style="width: 100%;">
                    <i class="icon icon-material-home" style="font-size: 40px; color: rgba(255, 255, 255, 0.84);"></i>
                    <h1>EQUOM Home</h1>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" style="width: 100%;">
                    <li><a href="{{ route('content_equom')}}">Administrar Contenido</a></li>
                    <li><a href="{{ route('images_equom')}}">Administrar Imagenes</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="btn-group" style="width: 100%;">
                <a href="javascript:void(0)" class="btn btn-material-teal btn-lg withripple dropdown-toggle" data-toggle="dropdown" style="width: 100%;" title="Administrar Contenido">
                    <i class="icon icon-material-wifi-tethering2" style="font-size: 40px; color: rgba(255, 255, 255, 0.84);"></i>
                    <h1>EQUOM Presencial</h1>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" style="width: 100%;">
                    <li><a href="{{ route('content_equom_presencial')}}">Administrar Contenido</a></li>
                    <li><a href="{{ route('images_equompresencial')}}">Administrar Imagenes</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="btn-group" style="width: 100%;">
                <a href="javascript:void(0)" class="btn btn-primary btn-lg withripple dropdown-toggle" data-toggle="dropdown" title="Administración de CompuBox" style="width: 100%;">
                    <i class="icon icon-material-laptop" style="font-size: 40px; color: rgba(255, 255, 255, 0.84);"></i>
                    <h1>CompuBox</h1>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" style="width: 100%;">
                    <li><a href="{{ route('ventas')}}">Administrar Ventas</a></li>
                    <li><a href="{{ route('content_compubox')}}">Administrar Contenido</a></li>
                    <li><a href="{{ route('images_compubox')}}">Administrar Imagenes</a></li>
                    @if(isadmin())
                        <li><a href="{{ route('administrar_producto')}}">Administrar Producto</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

@stop