@extends('layout_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 columns">
            <a  class="btn btn-primary" href="{{ route ('adduserform') }}">Agregar</a>
            <table id="users">
                <thead>
                <tr>
                    <th width="30%">Nombre</th>
                    <th width="30%">Correo</th>
                    <th width="20%">Tipo</th>
                    <th width="10%">Editar</th>
                    <th width="10%">Eliminar</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->full_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->user_type }}</td>
                        <td>
                            @if($user->user_type != 'master')
                                <a href="{{ route('updateuserform', [$user->id]) }}" class="btn btn-primary">Editar</a>
                            @endif
                        </td>
                        <td>
                            @if($user->user_type != 'master')
                                <a href="javascript:void(0)" onclick="eliminar({{$user->id}});" class="btn btn-danger">Eliminar</a>
                            @endif
                            </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('js-content')
<script type="text/javascript" charset="utf-8">

    function eliminar(id)
    {
    $.ajax({
            url: "{{route('drop_user')}}",
            async: false,
            data: {'id':id},
            type: "POST"
        }).done(function(data)
        {
            location.reload();
        });
    }
    $(document).ready(function() {
        $('#users').dataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/380cb78f450/i18n/Spanish.json"
            }
        });
    } );
</script>
@stop