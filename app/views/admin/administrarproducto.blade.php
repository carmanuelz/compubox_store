@extends('layout_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 columns">
            {{ Form::model($producto, ['route'=>'update_producto', 'method' => 'PUT', 'class' => 'form-horizontal']) }}
                <fieldset>
                    <legend>Administrar CompuBox</legend>
                    <div class="form-group">
                        {{ Form::label('descripcion', 'Descripción', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('descripcion', null,['class' => 'form-control']) }}
                            {{ $errors->first('descripcion','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('so', 'SO', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('so', null,['class' => 'form-control']) }}
                            {{ $errors->first('email','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('almacenamiento', 'Almacenamiento', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('almacenamiento', null,['class' => 'form-control']) }}
                            {{ $errors->first('almacenamiento','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('memorias', 'Memoria RAM', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('memorias', null,['class' => 'form-control']) }}
                            {{ $errors->first('memorias','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('cpu', 'CPU', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('cpu', null,['class' => 'form-control']) }}
                            {{ $errors->first('cpu','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('gpu', 'GPU', array('class' => 'col-lg-3 control-label')) }}
                        <div class="col-lg-9">
                            {{ Form::text('gpu', null,['class' => 'form-control']) }}
                            {{ $errors->first('gpu','<small class="error">:message</small>') }}
                        </div>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <a href="{{ route('admin_home')}}" class="btn btn-default withripple">Cancelar</a>
                        <button class="btn btn-material-teal withripple">Actualizar</button>
                    </div>
                </fieldset>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop